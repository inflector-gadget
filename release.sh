#!/bin/bash
V="$(git describe --always --tags --dirty=+ | sed 's|^v||')"
S=inflector-gadget-${V}
W=${S}-win
mkdir -p ${S} ${W}
make SYSTEM=windows ARCH=armv7 clean all
make SYSTEM=windows ARCH=aarch64 clean all
make SYSTEM=windows ARCH=i686 clean all
make SYSTEM=windows ARCH=x86_64 clean all
armv7-w64-mingw32-strip inflector-gadget.armv7.exe
aarch64-w64-mingw32-strip inflector-gadget.aarch64.exe
i686-w64-mingw32-strip inflector-gadget.i686.exe
x86_64-w64-mingw32-strip inflector-gadget.x86_64.exe
cp -avi inflector-gadget.armv7.exe ${W}
cp -avi inflector-gadget.aarch64.exe ${W}
cp -avi inflector-gadget.i686.exe ${W}
cp -avi inflector-gadget.x86_64.exe ${W}
cp -avi *.md ${W}
cp -avi *.md *.glsl *.cc *.h Makefile* *.sh ${S}
tar --create --verbose --owner=0 --group=0 --bzip --file ${S}.tar.bz2 ${S}/*
zip -r ${W}.zip ${W}/*
gpg -b ${S}.tar.bz2
gpg -b ${W}.zip
