#include "state.h"

struct state S;

void activate_keyframe()
{
  if (S.playing)
    return;
  if (0 <= S.s.keyframe && S.s.keyframe < int(S.s.keyframes.size()))
  {
    S.s.inflections = S.s.keyframes[S.s.keyframe];
    S.s.inflection = S.s.inflections.size();
  }
}

void add_keyframe()
{
  if (S.playing)
    return;
  if (S.s.keyframes.size() > 0)
    S.s.keyframe++;
  else
    S.s.keyframe = 0;
  S.s.keyframes.insert(S.s.keyframes.begin() + S.s.keyframe, S.s.inflections);
}

void delete_keyframe()
{
  if (S.playing)
    return;
  if (0 <= S.s.keyframe && S.s.keyframe < int(S.s.keyframes.size()))
  {
    S.s.keyframes.erase(S.s.keyframes.begin() + S.s.keyframe);
    if (S.s.keyframe >= int(S.s.keyframes.size()))
    {
      S.s.keyframe = 0;
    }
  }
  activate_keyframe();
}

void next_keyframe()
{
  S.s.keyframe++;
  if (S.s.keyframe >= int(S.s.keyframes.size()))
    S.s.keyframe = 0;
  activate_keyframe();
}

void previous_keyframe()
{
  S.s.keyframe--;
  if (S.s.keyframe < 0)
  {
    if (S.s.keyframes.size() > 0)
      S.s.keyframe = S.s.keyframes.size() - 1;
    else
      S.s.keyframe = 0;
  }
  activate_keyframe();
}

void faster_animation(bool much)
{
  S.s.speed *= much ? 2.0 : 1.0905077326652577; // 2**(1/8)
  if (isinf(S.s.speed))
    S.s.speed = 1.0 / 64;
}

void slower_animation(bool much)
{
  S.s.speed /= much ? 2.0 : 1.0905077326652577; // 2**(1/8)
  if (S.s.speed == 0)
    S.s.speed = 1.0 / 64;
}

void update_animation()
{
  if (S.is_stop_motion)
  {
    S.aindex = fmod(S.aindex + S.s.speed, S.s.inflections.size() + 1);
    S.s.inflection = int(floor(S.aindex));
  }
  else
  {
    if (S.s.keyframes.size() > 0)
    {
      S.aindex = fmod(S.aindex + S.s.speed, S.s.keyframes.size());
      int ix1 = int(floor(S.aindex)) % S.s.keyframes.size();
      int ix0 = (ix1 - 1 + S.s.keyframes.size()) % S.s.keyframes.size();
      int ix2 = (ix1 + 1) % S.s.keyframes.size();
      int ix3 = (ix2 + 1) % S.s.keyframes.size();
      double t = S.aindex - floor(S.aindex);
      double c0 = t * t * (2 - t) - t;
      double c1 = t * t * (3 * t - 5) + 2;
      double c2 = t * t * (4 - 3 * t) + t;
      double c3 = t * t * (t - 1);
      const std::vector<complex> &f0 = S.s.keyframes[ix0];
      const std::vector<complex> &f1 = S.s.keyframes[ix1];
      const std::vector<complex> &f2 = S.s.keyframes[ix2];
      const std::vector<complex> &f3 = S.s.keyframes[ix3];
      int s0 = f0.size();
      int s1 = f1.size();
      int s2 = f2.size();
      int s3 = f3.size();
      int s = std::min(std::min(std::min(s0, s1), s2), s3);
      S.s.inflection = s;
      S.s.inflections.resize(s);
      for (int i = 0; i < s; ++i)
        S.s.inflections[i] = 0.5 * (c0 * f0[i] + c1 * f1[i] + c2 * f2[i] + c3 * f3[i]);
    }
  }
}

void add_point(complex point)
{
  if (S.s.inflection < int(S.s.inflections.size()))
  {
    S.s.inflections[S.s.inflection] = point;
  }
  else
  {
    S.s.inflections.push_back(point);
  }
  S.s.inflection++;
}

void undo_add_point()
{
  if (S.s.inflection <= 0)
    return;
  S.s.inflection--;
}

void redo_add_point()
{
  if (S.s.inflection >= S.max_count)
    return;
  S.s.inflection++;
}
