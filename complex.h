#pragma once

#include <string>
#include <mpc.h>

// more precision than the dynamic range of the deltas is pointless
// the dynamic range of float is the limiting factor
// increase to 1024 if double perturbation is added
// make runtime adjustable if floatexp (huge dynamic range) is ever used
#define PREC 128
#define RNDR MPFR_RNDN
#define RNDC MPC_RNDNN

struct complex
{
  mpc_t data;

  complex()
  {
    mpc_init2(data, PREC);
  }

  ~complex()
  {
    mpc_clear(data);
  }

  complex(int c)
  : complex()
  {
    mpc_set_si_si(data, c, 0, RNDC);
  }

  complex(double c)
  : complex()
  {
    mpc_set_d_d(data, c, 0, RNDC);
  }

  complex(dvec2 c)
  : complex()
  {
    mpc_set_d_d(data, c.x, c.y, RNDC);
  }

  complex(const complex &c)
  : complex()
  {
    mpc_set(data, c.data, RNDC);
  }

  complex(const char *s)
  : complex()
  {
    mpc_set_str(data, s, 10, RNDC);
  }

  complex(const std::string &s)
  : complex(s.c_str())
  {
  }

  explicit operator dvec2() const
  {
    dvec2 r;
    r.x = mpfr_get_d(mpc_realref(data), RNDR);
    r.y = mpfr_get_d(mpc_imagref(data), RNDR);
    return r;
  }

  explicit operator vec2() const
  {
    vec2 r;
    r.x = mpfr_get_d(mpc_realref(data), RNDR);
    r.y = mpfr_get_d(mpc_imagref(data), RNDR);
    return r;
  }

  explicit operator std::string() const
  {
    char *s = mpc_get_str(10, 0, data, RNDC);
    std::string r(s);
    mpc_free_str(s);
    return r;
  }

  complex &operator =(const complex &that)
  {
    mpc_set(data, that.data, RNDC);
    return *this;
  }

  complex &operator +=(const complex &that)
  {
    mpc_add(data, data, that.data, RNDC);
    return *this;
  }

  complex &operator -=(const complex &that)
  {
    mpc_sub(data, data, that.data, RNDC);
    return *this;
  }

  complex &operator *=(const complex &that)
  {
    mpc_mul(data, data, that.data, RNDC);
    return *this;
  }

};

inline complex operator+(const complex &a, const complex &b)
{
  complex r;
  mpc_add(r.data, a.data, b.data, RNDC);
  return r;
}

inline complex operator-(const complex &a, const complex &b)
{
  complex r;
  mpc_sub(r.data, a.data, b.data, RNDC);
  return r;
}

inline complex operator*(const complex &a, const complex &b)
{
  complex r;
  mpc_mul(r.data, a.data, b.data, RNDC);
  return r;
}

inline complex sqr(const complex &a)
{
  complex r;
  mpc_sqr(r.data, a.data, RNDC);
  return r;
}

inline bool operator!=(const complex &a, const complex &b)
{
  return ! (mpfr_equal_p(mpc_realref(a.data), mpc_realref(b.data)) && mpfr_equal_p(mpc_imagref(a.data), mpc_imagref(b.data)));
}
