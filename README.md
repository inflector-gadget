inflector-gadget
================

Inflection mapping gadget for complex quadratic polynomials.

keyboard controls for navigation
--------------------------------

        ESC, Q  quit
        H       show this help
        F11     toggle fullscreen
        HOME    reset view
        UP      zoom in about cursor (faster with SHIFT)
        DOWN    zoom out about cursor (faster with SHIFT)
        PGUP    zoom in about center (faster with SHIFT)
        PGDOWN  zoom out about center (faster with SHIFT)
        1-9     zoom in to quadrant
        0       zoom out
        M       toggle Mandelbrot overlay
        C       toggle centering mode
        N       increase iteration count (decrease with SHIFT)
        J       increase sample count (reset with SHIFT)
        U       increase image undersampling (reset with SHIFT)
        O       increase image oversampling (reset with SHIFT)
        I       reset image sampling ratio
        A       randomize accent colour (reset with SHIFT)
        B       randomize decomposition colours (reset with SHIFT)
        F       use single precision without perturbation (default)
        D       toggle double precision (requires OpenGL 4.0+)
        G       toggle perturbation (requires OpenGL 4.3+)
        R       toggle reference point for perturbation
        -       undo add inflection point (remove all with SHIFT)
        =       redo add inflection point
        T       change colour theme
        S       save screenshot (in PNG format)

keyboard controls for animation
-------------------------------

        SPACE   add keyframe
        DELETE  delete keyframe
        LEFT    activate previous keyframe
        RIGHT   activate next keyframe
        ENTER   play animation (press again to stop)
        [       play slower (more with SHIFT)
        ]       play faster (more with SHIFT)
        P       reset speed to default (set to 1.0 with SHIFT)
        V       (without SHIFT) toggle video mode (stopframe, keyframe)
        V       (with SHIFT) save video sequence

mouse controls
--------------

        WHEEL   zoom about mouse cursor position (faster with SHIFT)
        LEFT    (without SHIFT) add inflection point
        LEFT    (with SHIFT) set reference point for perturbation
        RIGHT   undo add inflection point
        MIDDLE  recenter window about mouse cursor position

video output
------------

SHIFT V key saves an image sequence (in PNG format), unless the
`IG_VIDEO` environment variable is set, in which case a sequence of
uncompressed PPM images are piped to the command with the first `%s`
replaced by the output filename (which will have no extension).

Example using `ffmpeg`:

        export IG_VIDEO="ffmpeg -i - -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -an -movflags +faststart -y '%s.mp4'"
        ./inflector-gadget
        # press SHIFT V to record to MP4

security
--------

Settings files passed on the command line may set extreme resolutions
or iteration counts.  To mitigate the problems this might cause, you
can append another settings file to reconfigure.

Example contents of `failsafe.ig.toml`:

        width = 640
        height = 360
        fullscreen = false
        oversample = 1
        undersample = 1
        samples = 1
        iteratons = 256
        algorithm = 0

Example invocation:

        ./inflector-gadget untrusted.ig.toml failsafe.ig.toml

build
-----

Developed and tested on Debian Bookworm.

        sudo apt install \
            build-essential \
            git \
            libglew-dev \
            libsdl2-dev \
            libpng-dev \
            libtoml11-dev \
            libmpc-dev
        git clone https://code.mathr.co.uk/inflector-gadget.git
        cd inflector-gadget
        make
        make SYSTEM=windows
        make SYSTEM=windows ARCH=i686

You can install Windows dependencies for MINGW and LLVM-MINGW with
<https://mathr.co.uk/web/build-scripts.html>

legal
-----

inflector-gadget 0.6+git (GPL) 2023 Claude Heiland-Allen

inflector-gadget is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

inflector-gadget is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with inflector-gadget.  If not, see <http://www.gnu.org/licenses/>.
