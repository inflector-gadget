#pragma once

#include <string>

int save_png(const char *filename, const unsigned char *rgb, int width, int height, const char *comment);
std::string read_png_comment(const char *filename);
