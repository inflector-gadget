#include "settings.h"
#include "shader.h"

#include "common_vert.glsl.c"
#include "common_frag.glsl.c"
#include "plain_frag.glsl.c"
#include "perturb_frag.glsl.c"

static void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *) malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

static void debug_shader(GLuint shader, GLenum type, int count, const char **source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *) malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n", type_str, info ? info : "(no info log)");
    for (int i = 0; i < count; ++i)
    {
      fprintf(stderr, "%s\n", source[i]);
    }
  }
  if (info) {
    free(info);
  }
}

static GLuint vertex_fragment_shader(const char *version, const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    const char *vert_sources[3] = { version, "\n", vert };
    glShaderSource(shader, 3, vert_sources, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, 3, vert_sources);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *frag_sources[4] = { version, "\n", common_frag, frag };
    glShaderSource(shader, 4, frag_sources, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, 4, frag_sources);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

void update_shader(const struct shader &d, const struct settings &s, dvec2 aspect, GLuint *ssbo)
{
  switch (d.algorithm)
  {
    case algorithm_double_plain:
    case algorithm_double_perturb:
    {
      glUseProgram(d.program);
      glUniform1i (d.centering, s.centering);
      dvec2 center = dvec2(s.center);
      glUniform2dv(d.center, 1, &center[0]);
      dvec2 radius = dvec2(s.radius, 0.0);
      glUniform2dv(d.radius, 1, &radius[0]);
      glUniform2dv(d.aspect, 1, &aspect[0]);
      glUniform1i (d.mandelbrot, s.mandelbrot);
      glUniform1i (d.count, s.inflection);
      glUniform1i (d.iterations, s.iterations);
      glUniform1i (d.samples, s.samples);
      glUniform4fv(d.colours, COLOURS, &s.themes[s.theme].colours[0][0]);
      if (d.algorithm == algorithm_double_perturb)
      {
        center = s.reference_set ? dvec2(s.center - s.reference) : dvec2(0.0, 0.0);
        glUniform2dv(d.center, 1, &center[0]);
        center = s.reference_set ? dvec2(s.reference) : dvec2(s.center);
        glUniform2dv(d.center0, 1, &center[0]);
      }
      if (s.inflection > 0)
      {
        /* calculate escape radii */
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_ER]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(double) * s.inflection, nullptr, GL_STREAM_DRAW);
        double *er = (double *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
        if (er)
        {
          double er0 = 2.0;
          double c = length(dvec2(s.inflections[0]));
          double e = (1 + 2 * c) / 2;
          er0 = sqrt(e * e - c * c + c) + e;
          er0 = fmax(er0, 2.0);
          er[s.inflection - 1] = er0 * er0;
          for (int i = 1; i < s.inflection; ++i)
          {
            double c = length(dvec2(s.inflections[i]));
            er0 = fmax(2.0, sqrt(er0 + c) + c);
            er[s.inflection - i - 1] = er0 * er0;
          }
          glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
        }
        glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_ER, ssbo[SSBO_ER], 0, sizeof(double) * s.inflection);
      }
      if (d.algorithm == algorithm_double_plain)
      {
        /* upload inflection as VEC2 */
        if (s.inflection > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_INFLECTION]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(dvec2) * s.inflection, nullptr, GL_STREAM_DRAW);
          dvec2 *inflections = (dvec2 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (inflections)
          {
            for (int i = 0; i < s.inflection; ++i)
            {
              inflections[i] = dvec2(s.inflections[s.inflection - 1 - i]);
            }
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_INFLECTION, ssbo[SSBO_INFLECTION], 0, sizeof(dvec2) * s.inflection);
        }
      }
      else
      {
        /* upload perturbation data */
        complex C = s.reference_set ? s.reference : s.center;
        if (s.inflection > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_C_ORBIT]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(dvec4) * s.inflection, nullptr, GL_STREAM_DRAW);
          dvec4 *C_orbit = (dvec4 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (C_orbit)
          {
#if 0
            bool escaped = false;
#endif
            for (int i = 0; i < s.inflection; ++i)
            {
#if 0
              dvec2 dC = dvec2(C);
              if (dot(dC, dC) > er[MAXCOUNT - s.inflection + i])
              {
               escaped = true;
              }
#endif
              complex f = s.inflections[s.inflection - i - 1];
              if (s.centering && i == 0)
              {
                C += f;
              }
              C_orbit[i] = dvec4(dvec2(C), dvec2(C - f));
              C = sqr(C - f) + f;
            }
#if 0
            if (escaped)
            {
              fprintf(stderr, "reference escaped early, expect glitches\n");
            }
#endif
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_C_ORBIT, ssbo[SSBO_C_ORBIT], 0, sizeof(dvec4) * s.inflection);
        }
        if (s.iterations > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_Z_ORBIT]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(dvec4) * s.iterations, nullptr, GL_STREAM_DRAW);
          complex ZC = s.inflection != 0 ? C : complex(0);
          complex Z0 = complex(0);
          complex c = s.inflection != 0 ? s.inflections[0] : C;
          dvec4 *Z = (dvec4 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (Z)
          {
            for (int i = 0; i < s.iterations; ++i)
            {
              Z[i] = dvec4(dvec2(ZC), dvec2(Z0));
              ZC = sqr(ZC) + c;
              Z0 = sqr(Z0) + c;
            }
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_Z_ORBIT, ssbo[SSBO_Z_ORBIT], 0, sizeof(dvec4) * s.iterations);
        }
      }
      break;
    }
    case algorithm_float_plain:
    case algorithm_float_perturb:
    {
      vec2 center = vec2(s.center);
      vec2 radius = vec2(s.radius, 0.0);
      vec2 faspect = aspect;
      glUseProgram(d.program);
      glUniform1i (d.centering, s.centering);
      glUniform2fv(d.center, 1, &center[0]);
      glUniform2fv(d.radius, 1, &radius[0]);
      glUniform2fv(d.aspect, 1, &faspect[0]);
      glUniform1i (d.mandelbrot, s.mandelbrot);
      glUniform1i (d.count, s.inflection);
      glUniform1i (d.iterations, s.iterations);
      glUniform1i (d.samples, s.samples);
      glUniform4fv(d.colours, COLOURS, &s.themes[s.theme].colours[0][0]);
      if (d.algorithm == algorithm_float_perturb)
      {
        center = s.reference_set ? vec2(s.center - s.reference) : vec2(0.0, 0.0);
        glUniform2fv(d.center, 1, &center[0]);
        center = s.reference_set ? vec2(s.reference) : vec2(s.center);
        glUniform2fv(d.center0, 1, &center[0]);
      }
      if (s.inflection > 0)
      {
        /* calculate escape radii */
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_ER]);
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * s.inflection, nullptr, GL_STREAM_DRAW);
        float *er = (float *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
        if (er)
        {
          double er0 = 2.0;
          double c = length(dvec2(s.inflections[0]));
          double e = (1 + 2 * c) / 2;
          er0 = sqrt(e * e - c * c + c) + e;
          er0 = fmax(er0, 2.0);
          er[s.inflection - 1] = er0 * er0;
          for (int i = 1; i < s.inflection; ++i)
          {
            double c = length(dvec2(s.inflections[i]));
            er0 = fmax(2.0, sqrt(er0 + c) + c);
            er[s.inflection - i - 1] = er0 * er0;
          }
          glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_ER, ssbo[SSBO_ER], 0, sizeof(float) * s.inflection);
        }
      }
      if (d.algorithm == algorithm_float_plain)
      {
        /* upload inflection as VEC2 */
        if (s.inflection > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_INFLECTION]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec2) * s.inflection, nullptr, GL_STREAM_DRAW);
          vec2 *inflections = (vec2 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (inflections)
          {
            for (int i = 0; i < s.inflection; ++i)
            {
              inflections[i] = vec2(s.inflections[s.inflection - 1 - i]);
            }
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_INFLECTION, ssbo[SSBO_INFLECTION], 0, sizeof(vec2) * s.inflection);
        }
      }
      else
      {
        /* upload perturbation data */
        complex C = s.reference_set ? s.reference : s.center;
        if (s.inflection > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_C_ORBIT]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * s.inflection, nullptr, GL_STREAM_DRAW);
          vec4 *C_orbit = (vec4 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (C_orbit)
          {
#if 0
            bool escaped = false;
#endif
            for (int i = 0; i < s.inflection; ++i)
            {
#if 0
              dvec2 dC = dvec2(C);
              if (dot(dC, dC) > er[MAXCOUNT - s.inflection + i])
              {
               escaped = true;
              }
#endif
              complex f = s.inflections[s.inflection - i - 1];
              if (s.centering && i == 0)
              {
                C += f;
              }
              C_orbit[i] = vec4(vec2(C), vec2(C - f));
              C = sqr(C - f) + f;
            }
#if 0
            if (escaped)
            {
              fprintf(stderr, "reference escaped early, expect glitches\n");
            }
#endif
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_C_ORBIT, ssbo[SSBO_C_ORBIT], 0, sizeof(vec4) * s.inflection);
        }
        if (s.iterations > 0)
        {
          glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo[SSBO_Z_ORBIT]);
          glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(vec4) * s.iterations, nullptr, GL_STREAM_DRAW);
          complex ZC = s.inflection != 0 ? C : complex(0);
          complex Z0 = complex(0);
          complex c = s.inflection != 0 ? s.inflections[0] : C;
          vec4 *Z = (vec4 *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
          if (Z)
          {
            for (int i = 0; i < s.iterations; ++i)
            {
              Z[i] = dvec4(dvec2(ZC), dvec2(Z0));
              ZC = sqr(ZC) + c;
              Z0 = sqr(Z0) + c;
            }
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
          }
          glBindBufferRange(GL_SHADER_STORAGE_BUFFER, SSBO_Z_ORBIT, ssbo[SSBO_Z_ORBIT], 0, sizeof(vec4) * s.iterations);
        }
      }
      break;
    }
  }
}

struct shader compile_shader(const char *version, enum shader_algorithm algorithm)
{
  struct shader d;
  d.algorithm = algorithm;
  switch (algorithm)
  {
    case algorithm_float_plain:
    case algorithm_double_plain:
      d.program = vertex_fragment_shader(version, common_vert, plain_frag);
      break;
    case algorithm_float_perturb:
    case algorithm_double_perturb:
      d.program = vertex_fragment_shader(version, common_vert, perturb_frag);
      d.center0 = glGetUniformLocation(d.program, "center0");
      break;
  }
  d.centering  = glGetUniformLocation(d.program, "centering");
  d.center     = glGetUniformLocation(d.program, "center");
  d.radius     = glGetUniformLocation(d.program, "radius");
  d.aspect     = glGetUniformLocation(d.program, "aspect");
  d.mandelbrot = glGetUniformLocation(d.program, "mandelbrot");
  d.count      = glGetUniformLocation(d.program, "count");
  d.iterations = glGetUniformLocation(d.program, "iterations");
  d.samples    = glGetUniformLocation(d.program, "samples");
  d.colours    = glGetUniformLocation(d.program, "colours");
  return d;
}
