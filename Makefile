SYSTEM ?= linux

include Makefile.$(SYSTEM)

SHADERS := $(patsubst %.glsl,%.glsl.c,$(wildcard *.glsl))
OBJECTS := $(patsubst %.cc,%$(OEXT),$(wildcard *.cc))
DEPENDS := $(patsubst %.o,%.d,$(OBJECTS))

all: inflector-gadget$(EXEEXT)

clean:
	@echo "CLEAN" ; rm -f inflector-gadget$(EXEEXT) $(SHADERS) $(OBJECTS) $(DEPENDS)

.SUFFIXES:
.PHONY: all clean
.SECONDARY: $(SHADERS) $(OBJECTS)

inflector-gadget$(EXEEXT): $(OBJECTS)
	$(COMPILE) $(COMPILE_FLAGS) -o inflector-gadget$(EXEEXT) $(OBJECTS) $(LINK_FLAGS)

%$(OEXT): %.cc
	$(COMPILE) $(COMPILE_FLAGS) -o $@ -c $<

%.glsl.c: %.glsl s2c.sh
	bash s2c.sh $* < $< > $@

shader$(OEXT): $(SHADERS)
-include $(DEPENDS)
