#pragma once

#include "settings.h"
#include "shader.h"

struct state
{
  // uniform state
  struct settings s;
  // single, double, perturbation
  struct shader f;
  struct shader d;
  struct shader fp;
  struct shader dp;
  // other stuff
  int fb_width; // window framebuffer size in pixels
  int fb_height;
  GLuint texture;
  GLuint fbo;
  int fbo_width; // fbo framebuffer size in pixels
  int fbo_height;
  int max_count;
  GLuint ssbo[5]; // buffers ([0] unused)
  // image
  dvec2 aspect;
  unsigned char *ppm;
  int ppm_width; // in pixels
  int ppm_height;
  int time;
  int frame;
  // animation
  int stop_motion_inflection;
  double aindex;
  int aframes;
  // recording
  bool is_stop_motion;
  bool playing;
  bool recording;
  bool screenshot;
  FILE *video;
  bool running;
};

extern struct state S;

void activate_keyframe();
void add_keyframe();
void delete_keyframe();
void next_keyframe();
void previous_keyframe();
void faster_animation(bool much);
void slower_animation(bool much);
void update_animation();
void add_point(complex point);
void undo_add_point();
void redo_add_point();
