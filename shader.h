#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "settings.h"

struct shader
{
  enum shader_algorithm algorithm;
  // program
  GLuint program;
  // uniform location
  GLint centering;
  GLint center;
  GLint radius;
  GLint aspect;
  GLint mandelbrot;
  GLint count;
  GLint iterations;
  GLint samples;
  GLint colours;
  // perturbation
  GLint center0;
};

struct shader compile_shader(const char *version, enum shader_algorithm algorithm);

void update_shader(const struct shader &d, const struct settings &s, dvec2 aspect, GLuint *ssbo);
