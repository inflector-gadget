#include "settings.h"
#include "version.h"

#include <toml.hpp>

bool operator == (const struct theme &a, const struct theme &b)
{
  for (int c = 0; c < COLOURS; ++c)
  {
    if (a.colours[c] != b.colours[c]) return false;
  }
  return true;
}

const struct theme lighttheme =
{{vec4(0.0, 0.0, 0.0, 1.0)
, vec4(0.0, 0.0, 0.0, 1.0)
, vec4(1.0, 1.0, 1.0, 1.0)
, vec4(1.0, 1.0, 1.0, 1.0)
, vec4(0.5, 0.5, 0.5, 0.2)
, vec4(0.0, 0.5, 0.0, 1.0)
, vec4(1.0, 1.0, 1.0, 0.2)
, vec4(0.7, 0.7, 0.7, 0.2)
, vec4(1.0, 1.0, 1.0, 1.0)
}};

const struct theme darktheme =
{{vec4(1.0, 1.0, 1.0, 1.0)
, vec4(1.0, 1.0, 1.0, 1.0)
, vec4(0.0, 0.0, 0.0, 1.0)
, vec4(0.0, 0.0, 0.0, 1.0)
, vec4(0.5, 0.5, 0.5, 0.2)
, vec4(0.0, 0.5, 0.0, 1.0)
, vec4(0.0, 0.0, 0.0, 0.2)
, vec4(0.3, 0.3, 0.3, 0.2)
, vec4(0.0, 0.0, 0.0, 1.0)
}};

const struct settings default_settings =
{ complex(0)
, 2.0
, 256
, 0
, false
, 640
, 360
, 1
, 1
, true
, false
, algorithm_float_plain
, false
, complex(0)
, 0
, { lighttheme, darktheme }
, 0
, {}
, 0
, {}
, 1.0 / 64.0
};

std::istream &operator>>(std::istream &ifs, struct settings &s)
{
  auto t = toml::parse(ifs);
#define LOAD1(a) s.a = toml::find_or(t, #a, s.a);
#define LOAD2(a,b) s.a.b = toml::find_or(t, #a, #b, s.a.b);
  int64_t format = toml::find_or(t, "format", int64_t(0));
  if (format > 1)
  {
    fprintf(stderr, "WARNING: file format %d is newer than supported %d\n", int(format), 1);
  }
  if (format >= 1)
  {
    s.center = complex(toml::find_or(t, "center", std::string(s.center)));
  }
  else if (format == 0)
  {
    double x = toml::find_or(t, "center", "x", 0.0/0.0);
    if (! isnan(x))
    {
      mpfr_set_d(mpc_realref(s.center.data), x, RNDR);
    }
    double y = toml::find_or(t, "center", "y", 0.0/0.0);
    if (! isnan(y))
    {
      mpfr_set_d(mpc_imagref(s.center.data), y, RNDR);
    }
  }
  LOAD1(radius)
  LOAD1(iterations)
  LOAD1(samples)
  LOAD1(fullscreen)
  LOAD1(width)
  LOAD1(height)
  LOAD1(oversample)
  LOAD1(undersample)
  LOAD1(centering)
  LOAD1(mandelbrot)
  s.algorithm = (enum shader_algorithm)(toml::find_or(t, "algorithm", int64_t(s.algorithm)));
  s.reference = complex(toml::find_or(t, "reference", std::string(s.reference)));
  LOAD1(reference_set)

  // themes
  LOAD1(theme)
  auto themes = toml::find_or(t, "themes", std::vector<toml::value>());
  if (themes.size())
  {
  s.themes.resize(themes.size());
  for (int th = 0; th < int(themes.size()); ++th)
  {
#define LOADC1(a,b,c,d) s.themes[a].colours[COLOUR_ ## c].d = toml::find_or(themes[a], #b, #d, double(s.themes[a].colours[COLOUR_ ## c].d));
#define LOADC(ix,field,def) LOADC1(ix,field,def,r) LOADC1(ix,field,def,g) LOADC1(ix,field,def,b) LOADC1(ix,field,def,a)
    LOADC(th, interior, INTERIOR)
    LOADC(th, boundary, BOUNDARY)
    LOADC(th, exterior1, EXTERIOR1)
    LOADC(th, exterior2, EXTERIOR2)
    LOADC(th, mandelbrot_interior, MINTERIOR)
    LOADC(th, mandelbrot_boundary, MBOUNDARY)
    LOADC(th, mandelbrot_exterior1, MEXTERIOR1)
    LOADC(th, mandelbrot_exterior2, MEXTERIOR2)
    LOADC(th, accent, ACCENT)
#undef LOADC1
#undef LOADC
  }
  }
  if (s.themes.size() < 1) s.themes.push_back(lighttheme);
  if (s.themes.size() < 2) s.themes.push_back(darktheme);
  if (! (0 <= s.theme && s.theme < int(s.themes.size())))
  {
    s.theme = 0;
  }

  // inflections
  LOAD1(inflection)
  if (format >= 1)
  {
    auto inflections = toml::find_or(t, "inflections", std::vector<std::string>());
    if (inflections.size())
    {
      s.inflections.resize(inflections.size());
      for (int ix = 0; ix < int(s.inflections.size()); ++ix)
      {
        s.inflections[ix] = complex(inflections[ix]);
      }
    }
    if (! (0 <= s.inflection && s.inflection <= int(s.inflections.size())))
    {
      fprintf(stderr, "WARNING: inflection %d out of range, setting to %d...\n", int(s.inflection), int(inflections.size()));
      s.inflection = inflections.size();
    }
  }
  else if (format == 0)
  {
    auto inflections = toml::find_or(t, "inflections", std::vector<toml::value>());
    if (inflections.size())
    {
      s.inflections.resize(inflections.size());
      for (int ix = 0; ix < int(s.inflections.size()); ++ix)
      {
        dvec2 i;
        i.x = toml::find_or(inflections[ix], "x", 0.0);
        i.y = toml::find_or(inflections[ix], "y", 0.0);
        s.inflections[ix] = complex(i);
      }
    }
    if (! (0 <= s.inflection && s.inflection <= int(s.inflections.size())))
    {
      fprintf(stderr, "WARNING: inflection %d out of range, setting to %d...\n", int(s.inflection), int(inflections.size()));
      s.inflection = inflections.size();
    }
  }

  // animation keyframes
  LOAD1(keyframe)
  if (format >= 1)
  {
    auto keyframes = toml::find_or(t, "keyframes", std::map<std::string, std::vector<std::string>>());
    if (keyframes.size())
    {
      s.keyframes.resize(keyframes.size());
      for (int k = 0; k < int(s.keyframes.size()); ++k)
      {
        char ks[100];
        snprintf(ks, sizeof(ks), "%d", k);
        if (keyframes.find(ks) == keyframes.end())
        {
          s.keyframes[k].resize(0);
        }
        else
        {
          s.keyframes[k].resize(keyframes[ks].size());
          for (int ix = 0; ix < int(s.keyframes[k].size()); ++ix)
          {
            s.keyframes[k][ix] = complex(keyframes[ks][ix]);
          }
        }
      }
    }
    if (! (0 <= s.keyframe && s.keyframe < int(keyframes.size())))
    {
      s.keyframe = 0;
    }
  }
  else
  {
    auto keyframes = toml::find_or(t, "keyframes", std::map<std::string, std::vector<toml::value>>());
    if (keyframes.size())
    {
      s.keyframes.resize(keyframes.size());
      for (int k = 0; k < int(s.keyframes.size()); ++k)
      {
        char ks[100];
        snprintf(ks, sizeof(ks), "%d", k);
        if (keyframes.find(ks) == keyframes.end())
        {
          s.keyframes[k].resize(0);
        }
        else
        {
          s.keyframes[k].resize(keyframes[ks].size());
          for (int ix = 0; ix < int(s.keyframes[k].size()); ++ix)
          {
            dvec2 i;
            i.x = toml::find_or(keyframes[ks][ix], "x", 0.0);
            i.y = toml::find_or(keyframes[ks][ix], "y", 0.0);
            s.keyframes[k][ix] = complex(i);
          }
        }
      }
    }
    if (! (0 <= s.keyframe && s.keyframe < int(keyframes.size())))
    {
      s.keyframe = 0;
    }
  }
  LOAD1(speed)

#undef LOAD1
#undef LOAD2
  return ifs;
}

std::ostream &operator<<(std::ostream &ofs, const struct settings &p)
{
  struct settings q = default_settings;
  ofs << "program = " << toml::value("inflector-gadget") << "\n";
  ofs << "version = " << toml::value(inflector_gadget_version_string) << "\n";
  ofs << "format = " << toml::value(int64_t(1)) << "\n";
#define SAVE1(a) if (p.a != q.a) { ofs << #a << " = " << std::setw(70) << toml::value(p.a) << "\n"; }
#define SAVE2(a,b) if (p.a.b != q.a.b) { ofs << #a << "." << #b << " = " << std::setw(70) << toml::value(p.a.b) << "\n"; }
  ofs << std::setprecision(18);
#if 1
  if (p.center != q.center) { ofs << "center = " << std::setw(120) << toml::value((std::string)(p.center)) << "\n"; }
#else
  SAVE2(center, x)
  SAVE2(center, y)
#endif
  SAVE1(radius)
  SAVE1(iterations)
  SAVE1(samples)
  SAVE1(fullscreen)
  SAVE1(width)
  SAVE1(height)
  SAVE1(oversample)
  SAVE1(undersample)
  SAVE1(centering)
  SAVE1(mandelbrot)
  if (p.algorithm != q.algorithm) { ofs << "algorithm = " << std::setw(70) << toml::value(int64_t(p.algorithm)) << "\n"; }
#if 1
  if (p.reference != q.reference) { ofs << "reference = " << std::setw(120) << toml::value(std::string(p.reference)) << "\n"; }
#else
  SAVE2(reference, x)
  SAVE2(reference, y)
#endif
  SAVE1(reference_set)
  SAVE1(theme)
  SAVE1(inflection)
  SAVE1(keyframe)
  SAVE1(speed)
#undef SAVE1
#undef SAVE2

  // inflections
  ofs << std::setprecision(18);
  {
    toml::array inflections;
    for (auto i : p.inflections)
    {
      inflections.push_back(std::string(i));
    }
    std::map<std::string, toml::array> f;
    f["inflections"] = inflections;
    ofs << std::setw(120) << toml::value(f);
  }

  // keyframes
  ofs << std::setprecision(18);
  {
    std::map<std::string, std::vector<toml::value>> keyframes;
    int j = 0;
    for (auto i : p.keyframes)
    {
      std::vector<toml::value> keyframe;
      for (auto k : i)
      {
        keyframe.push_back(std::string(k));
      }
      char s[100];
      snprintf(s, sizeof(s), "%d", j++);
      keyframes[s] = keyframe;
    }
    std::map<std::string, std::map<std::string, std::vector<toml::value>>> f;
    f["keyframes"] = keyframes;
    ofs << std::setw(120) << toml::value(f);
  }

  // themes
  ofs << std::setprecision(5);
  if (! (p.themes == q.themes))
  {
    toml::array themes;
    for (auto t : p.themes)
    {
      std::map<std::string, toml::value> f;
#define SAVEC(name,def) { std::map<std::string, toml::value> c; c["r"] = t.colours[COLOUR_ ## def].r; c["g"] = t.colours[COLOUR_ ## def].g; c["b"] = t.colours[COLOUR_ ## def].b; c["a"] = t.colours[COLOUR_ ## def].a; f[#name] = c; }
      SAVEC(interior, INTERIOR)
      SAVEC(boundary, BOUNDARY)
      SAVEC(exterior1, EXTERIOR1)
      SAVEC(exterior2, EXTERIOR2)
      SAVEC(mandelbrot_interior, MINTERIOR)
      SAVEC(mandelbrot_boundary, MBOUNDARY)
      SAVEC(mandelbrot_exterior1, MEXTERIOR1)
      SAVEC(mandelbrot_exterior2, MEXTERIOR2)
      SAVEC(accent, ACCENT)
#undef SAVEC
      themes.push_back(f);
    }
    std::map<std::string, toml::array> f;
    f["themes"] = themes;
    ofs << toml::value(f);
  }

  return ofs;
}
