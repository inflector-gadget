#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fstream>
#include <sstream>
#include <GL/glew.h>
#include <SDL.h>

#include "png.h"
#include "settings.h"
#include "shader.h"
#include "state.h"
#include "version.h"

// https://stackoverflow.com/a/2072890
inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

void resize(SDL_Window *window)
{
      int fb_width, fb_height;
      SDL_GL_GetDrawableSize(window, &fb_width, &fb_height);
      S.fb_width = fb_width;
      S.fb_height = fb_height;
      glViewport(0, 0, fb_width, fb_height);
      int width, height;
      SDL_GetWindowSize(window, &width, &height);
      S.s.width = width;
      S.s.height = height;
      S.aspect = dvec2(S.s.width * 1.0 / S.s.height, 1.0);
}

void set_fullscreen(SDL_Window *window, bool fullscreen)
{
  SDL_SetWindowFullscreen(window, fullscreen);
  resize(window);
}

std::string make_filename(const char *extension, bool include_frame_number)
{
  char filename[256];
  time_t seconds_since_epoch = S.time;
  struct tm *t = localtime(&seconds_since_epoch);
  if (t)
  {
    // human-readable datetime format
    if (include_frame_number)
    {
      snprintf(filename, sizeof(filename), "%04d-%02d-%02d-%02d-%02d-%02d-%06d%s", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, S.frame, extension);
    }
    else
    {
      snprintf(filename, sizeof(filename), "%04d-%02d-%02d-%02d-%02d-%02d%s", 1900 + t->tm_year, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, extension);
    }
  }
  else
  {
    // cryptically-named fallback in case the localtime() function fails
    if (include_frame_number)
    {
      snprintf(filename, sizeof(filename), "%08x-%06d%s", (unsigned int) seconds_since_epoch, S.frame, extension);
    }
    else
    {
      snprintf(filename, sizeof(filename), "%08x%s", (unsigned int) seconds_since_epoch, extension);
    }
  }
  return filename;
}

static void save_screenshot()
{
  // in video mode, save settings only on first frame instead of every frame
  std::string filename = make_filename(".ig.toml", ! S.video);
  if (! S.video || S.frame == 0)
  {
    fprintf(stderr, "saving settings '%s'\n", filename.c_str());
    std::ofstream ofs(filename);
    ofs << S.s;
    ofs.close();
  }
  filename = make_filename(".ig.png", ! S.video);
  if (S.ppm_width * S.ppm_height < S.fbo_width * S.fbo_height)
  {
    if (S.ppm)
    {
      free(S.ppm);
      S.ppm = 0;
    }
    S.ppm_width = S.fbo_width;
    S.ppm_height = S.fbo_height;
  }
  if (! S.ppm)
  {
    S.ppm = (unsigned char *) malloc(S.ppm_width * S.ppm_height * 3);
  }
  if (S.ppm)
  {
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadPixels(0, 0, S.fbo_width, S.fbo_height, GL_RGB, GL_UNSIGNED_BYTE, S.ppm);
    if (S.video)
    {
      // write PPM to stream FIXME TODO check for errors
      fprintf(S.video, "P6\n%d %d\n255\n", S.fbo_width, S.fbo_height);
      for (int y = S.fbo_height - 1; y >= 0; --y)
      {
        fwrite(S.ppm + y * S.fbo_width * 3, S.fbo_width * 3, 1, S.video);
      }
      fflush(S.video);
    }
    if (! S.video || S.frame == 0)
    {
      // write PNG file
      fprintf(stderr, "saving image    '%s'\n", filename.c_str());
      std::ostringstream comment;
      comment << S.s;
      if (0 != save_png(filename.c_str(), S.ppm, S.fbo_width, S.fbo_height, comment.str().c_str()))
      {
        fprintf(stderr, "saving image   '%s' FAILED\n", filename.c_str());
      }
    }
  }
  else
  {
    fprintf(stderr, "saving image FAILED\n");
  }
  S.frame++;
}

static complex mouse_position(double x, double y)
{
  double w = S.s.width;
  double h = S.s.height;
  double dx = 2 * ((x + 0.5) / w - 0.5);
  double dy = 2 * (0.5 - (y + 0.5) / h);
  dvec2 p = dvec2(dx, dy);
  complex t = S.s.center + complex(S.s.radius * S.aspect * p);
  if (S.s.centering && S.s.inflection > 0)
    t += S.s.inflections[S.s.inflection - 1];
  return t;
}

complex mix(const complex &a, const complex &b, double t)
{
  return a + (b - a) * t;
}

static void zoom(double x, double y, double amount)
{
  double g = pow(0.5, amount / 5);
  complex center = S.s.center;
  if (S.s.centering && S.s.inflection > 0)
    center += S.s.inflections[S.s.inflection - 1];
  center = mix(mouse_position(x, y), center, g);
  if (S.s.centering && S.s.inflection > 0)
    center -= S.s.inflections[S.s.inflection - 1];
  S.s.center = center;
  S.s.radius *= g;
}

static void start_playback()
{
  int keyframes;
  if (S.is_stop_motion)
  {
    S.stop_motion_inflection = S.s.inflection;
    keyframes = S.s.inflections.size() + 1;
  }
  else
  {
    keyframes = S.s.keyframes.size();
  }
  S.aframes = round(keyframes / S.s.speed);
  S.s.speed = double(keyframes) / double(S.aframes);
  if (isnan(S.s.speed) || isinf(S.s.speed))
  {
    S.s.speed = 1.0 / 64;
  }
  S.aindex = -S.s.speed;
  S.playing = true;
}

double mouse_x = 0;
double mouse_y = 0;
void handle_event(SDL_Window *window, const SDL_Event &e)
{
  SDL_Keymod mods = SDL_GetModState();
  switch (e.type)
  {

    case SDL_WINDOWEVENT:
    {
      switch (e.window.event)
      {

    case SDL_WINDOWEVENT_CLOSE:
    {
      S.running = false;
      break;
    }

        case SDL_WINDOWEVENT_SIZE_CHANGED:
        {
          resize(window);
          break;
        }
      }
      break;
    }

    case SDL_KEYDOWN:
    {
      switch (e.key.keysym.sym)
      {
      case SDLK_q:
      case SDLK_ESCAPE:
        S.running = false;
        break;
      case SDLK_F11:
        S.s.fullscreen = ! S.s.fullscreen;
        set_fullscreen(window, S.s.fullscreen);
        break;
      // keyboard navigation
      case SDLK_HOME:
        S.s.center = complex(0);
        S.s.radius = 2.0;
        break;
      case SDLK_UP:
        zoom(mouse_x, mouse_y, (mods & KMOD_SHIFT) ? 10 : 1);
        break;
      case SDLK_DOWN:
        zoom(mouse_x, mouse_y, (mods & KMOD_SHIFT) ? -10 : -1);
        break;
      case SDLK_PAGEUP:   zoom(S.s.width * 0.5, S.s.height * 0.5, (mods & KMOD_SHIFT) ? 5 : 1);  break;
      case SDLK_PAGEDOWN: zoom(S.s.width * 0.5, S.s.height * 0.5, (mods & KMOD_SHIFT) ? -5 : -1);  break;
      case SDLK_1: case SDLK_KP_1: zoom(0, S.s.height, 5); break;
      case SDLK_2: case SDLK_KP_2: zoom(S.s.width * 0.5, S.s.height, 5); break;
      case SDLK_3: case SDLK_KP_3: zoom(S.s.width, S.s.height, 5); break;
      case SDLK_4: case SDLK_KP_4: zoom(0, S.s.height * 0.5, 5); break;
      case SDLK_5: case SDLK_KP_5: zoom(S.s.width * 0.5, S.s.height * 0.5, 5); break;
      case SDLK_6: case SDLK_KP_6: zoom(S.s.width, S.s.height * 0.5, 5); break;
      case SDLK_7: case SDLK_KP_7: zoom(0, 0, 5); break;
      case SDLK_8: case SDLK_KP_8: zoom(S.s.width * 0.5, 0, 5); break;
      case SDLK_9: case SDLK_KP_9: zoom(S.s.width, 0, 5); break;
      case SDLK_0: case SDLK_KP_0: zoom(S.s.width * 0.5, S.s.height * 0.5, -5); break;
      // other keyboard controls
      case SDLK_m:
        S.s.mandelbrot = ! S.s.mandelbrot;
        break;
      case SDLK_c:
        S.s.centering = ! S.s.centering;
        break;
      case SDLK_r:
        if (mods & KMOD_SHIFT)
        {
          S.s.reference = complex(0);
          S.s.reference_set = true;
        }
        else
        {
          S.s.reference_set = ! S.s.reference_set;
        }
        break;
      case SDLK_MINUS:
        if (mods & KMOD_SHIFT)
        {
          S.s.inflection = 0;
        }
        else
        {
          undo_add_point();
        }
        break;
      case SDLK_EQUALS:
        redo_add_point();
        break;
      case SDLK_d:
        switch (S.s.algorithm)
        {
          case algorithm_float_plain: S.s.algorithm = algorithm_double_plain; break;
          case algorithm_double_plain: S.s.algorithm = algorithm_float_plain; break;
          case algorithm_float_perturb: S.s.algorithm = algorithm_double_perturb; break;
          case algorithm_double_perturb: S.s.algorithm = algorithm_float_perturb; break;
        }
        break;
      case SDLK_f:
        S.s.algorithm = algorithm_float_plain;
        break;
      case SDLK_g:
      {
        switch (S.s.algorithm)
        {
          case algorithm_float_plain: S.s.algorithm = algorithm_float_perturb; break;
          case algorithm_double_plain: S.s.algorithm = algorithm_double_perturb; break;
          case algorithm_float_perturb: S.s.algorithm = algorithm_float_plain; break;
          case algorithm_double_perturb: S.s.algorithm = algorithm_double_plain; break;
        }
      }
      break;
      case SDLK_j:
        if (mods & KMOD_SHIFT)
        {
          S.s.samples = 0;
        }
        else
        {
          if (S.s.samples < 256) // FIXME
          {
            S.s.samples <<= 1;
            S.s.samples += ! S.s.samples;
          }
        }
        break;
      case SDLK_u:
        if (mods & KMOD_SHIFT)
        {
          S.s.undersample = 1;
        }
        else
        {
          S.s.undersample += 1;
        }
        break;
      case SDLK_o:
        if (mods & KMOD_SHIFT)
        {
          S.s.oversample = 1;
        }
        else
        {
          S.s.oversample += 1;
        }
        break;
      case SDLK_i:
        S.s.undersample = 1;
        S.s.oversample = 1;
        break;
      case SDLK_n:
        if (mods & KMOD_SHIFT)
        {
          if (S.s.iterations > (1 << 4)) // FIXME
          {
            S.s.iterations >>= 1;
          }
        }
        else
        {
          if (S.s.iterations < (1 << 24)) // FIXME
          {
            S.s.iterations <<= 1;
          }
        }
        break;
      case SDLK_s:
        // save after rendering
        S.screenshot = true;
        break;
      case SDLK_t:
        S.s.theme = (S.s.theme + 1) % S.s.themes.size();;
        break;
      case SDLK_a:
        if (mods & KMOD_SHIFT)
        {
          if (S.s.themes.size() > 0)
          S.s.themes[0].colours[COLOUR_ACCENT] = lighttheme.colours[COLOUR_ACCENT];
          if (S.s.themes.size() > 1)
          S.s.themes[1].colours[COLOUR_ACCENT] = darktheme.colours[COLOUR_ACCENT];
        }
        else
        {
          for (int c = 0; c < 4; ++c)
          {
            double x = 0;
            if (S.s.themes.size() > 0)
            S.s.themes[0].colours[COLOUR_ACCENT][c] = x = rand() / (double) RAND_MAX;
            if (S.s.themes.size() > 1)
            S.s.themes[1].colours[COLOUR_ACCENT][c] = x;
          }
        }
        break;
      case SDLK_b:
        if (mods & KMOD_SHIFT)
        {
          if (S.s.themes.size() > 0)
          {
          S.s.themes[0].colours[COLOUR_EXTERIOR1] = lighttheme.colours[COLOUR_EXTERIOR1];
          S.s.themes[0].colours[COLOUR_EXTERIOR2] = lighttheme.colours[COLOUR_EXTERIOR2];
          }
          if (S.s.themes.size() > 1)
          {
          S.s.themes[1].colours[COLOUR_EXTERIOR1] = darktheme.colours[COLOUR_EXTERIOR1];
          S.s.themes[1].colours[COLOUR_EXTERIOR2] = darktheme.colours[COLOUR_EXTERIOR2];
          }
        }
        else
        {
          for (int c = 0; c < 3; ++c)
          {
            double x = 0;
            if (S.s.themes.size() > 0)
            S.s.themes[0].colours[COLOUR_EXTERIOR1][c] = x = rand() / (double) RAND_MAX;
            if (S.s.themes.size() > 1)
            S.s.themes[1].colours[COLOUR_EXTERIOR1][c] = x;
            if (S.s.themes.size() > 0)
            S.s.themes[0].colours[COLOUR_EXTERIOR2][c] = x = rand() / (double) RAND_MAX;
            if (S.s.themes.size() > 1)
            S.s.themes[1].colours[COLOUR_EXTERIOR2][c] = x;
          }
        }
        break;
      case SDLK_SPACE:
        add_keyframe();
        break;
      case SDLK_DELETE:
        delete_keyframe();
        break;
      case SDLK_LEFT:
        previous_keyframe();
        break;
      case SDLK_RIGHT:
        next_keyframe();
        break;
      case SDLK_RETURN:
      case SDLK_KP_ENTER:
        S.playing = ! S.playing;
        if (S.playing)
        {
          start_playback();
        }
        else
        {
          if (S.is_stop_motion)
          {
            S.s.inflection = S.stop_motion_inflection;
          }
          else
          {
            activate_keyframe();
          }
        }
        break;
      case SDLK_RIGHTBRACKET:
        faster_animation(mods & KMOD_SHIFT);
        break;
      case SDLK_LEFTBRACKET:
        slower_animation(mods & KMOD_SHIFT);
        break;
      case SDLK_p:
        if (mods & KMOD_SHIFT)
        {
          S.s.speed = 1.0;
        }
        else
        {
          S.s.speed = 1.0 / 64;
        }
        break;
      case SDLK_v:
        if (mods & KMOD_SHIFT)
        {
          S.time = time(0);
          S.frame = 0;
          start_playback();
          S.recording = true;
          const char *video = getenv("IG_VIDEO");
          if (video)
          {
            std::string filename = make_filename(".ig", false);
            size_t bytes = strlen(video) + strlen(filename.c_str()) + 256;
            char *command = (char *) malloc(bytes);
            if (command)
            {
              snprintf(command, bytes, video, filename.c_str());
              S.video = popen(command, "w");
              if (S.video)
              {
                fprintf(stderr, "saving video '%s'\n", command);
              }
              else
              {
                fprintf(stderr, "saving video '%s' FAILED\n", command);
              }
              free(command);
            }
            else
            {
              fprintf(stderr, "saving video FAILED\n");
            }
          }
        }
        else
        {
          if (! S.playing)
          {
            S.is_stop_motion = ! S.is_stop_motion;
          }
        }
        break;
      case SDLK_h:
        fprintf(stderr,
          "keyboard controls for navigation:\n"
          "\tESC, Q\tquit\n"
          "\tH\tshow this help\n"
          "\tF11\ttoggle fullscreen\n"
          "\tHOME\treset view\n"
          "\tUP\tzoom in about cursor (faster with SHIFT)\n"
          "\tDOWN\tzoom out about cursor (faster with SHIFT)\n"
          "\tPGUP\tzoom in about center (faster with SHIFT)\n"
          "\tPGDOWN\tzoom out about center (faster with SHIFT)\n"
          "\t1-9\tzoom in to quadrant\n"
          "\t0\tzoom out\n"
          "\tM\ttoggle Mandelbrot overlay\n"
          "\tC\ttoggle centering mode\n"
          "\tN\tincrease iteration count (decrease with SHIFT)\n"
          "\tJ\tincrease sample count (reset with SHIFT)\n"
          "\tU\tincrease image undersampling (reset with SHIFT)\n"
          "\tO\tincrease image oversampling (reset with SHIFT)\n"
          "\tI\treset image sampling ratio\n"
          "\tA\trandomize accent colour (reset with SHIFT)\n"
          "\tB\trandomize decomposition colours (reset with SHIFT)\n"
          "\tF\tuse single precision without perturbation (default)\n"
          "\tD\ttoggle double precision (requires OpenGL 4.0+)\n"
          "\tG\ttoggle perturbation (requires OpenGL 4.3+)\n"
          "\tR\ttoggle reference point for perturbation\n"
          "\t-\tundo add inflection point (remove all with SHIFT)\n"
          "\t=\tredo add inflection point\n"
          "\tT\tchange colour theme\n"
          "\tS\tsave screenshot (in PNG format)\n"
          "keyboard controls for animation:\n"
          "\tSPACE\tadd keyframe\n"
          "\tDELETE\tdelete keyframe\n"
          "\tLEFT\tactivate previous keyframe\n"
          "\tRIGHT\tactivate next keyframe\n"
          "\tENTER\tplay animation (press again to stop)\n"
          "\t[\tplay slower (more with SHIFT)\n"
          "\t]\tplay faster (more with SHIFT)\n"
          "\tP\treset speed to default (set to 1.0 with SHIFT)\n"
          "\tV\t(without SHIFT) toggle video mode (stopframe, keyframe)\n"
          "\tV\t(with SHIFT) save video sequence\n"
          "mouse controls:\n"
          "\tWHEEL\tzoom about mouse cursor position (faster with SHIFT)\n"
          "\tLEFT\t(without SHIFT) add inflection point\n"
          "\tLEFT\t(with SHIFT) set reference point for perturbation\n"
          "\tRIGHT\tundo add inflection point\n"
          "\tMIDDLE\trecenter window about mouse cursor position\n"
        );
        break;
    }
    break;
    }

    case SDL_MOUSEBUTTONUP:
    {
      switch (e.button.button)
      {
      case SDL_BUTTON_LEFT:
        if (mods & KMOD_SHIFT)
        {
          S.s.reference = mouse_position(e.button.x, e.button.y);
          if (S.s.centering && S.s.inflection > 0)
            S.s.reference -= S.s.inflections[S.s.inflection - 1];
          S.s.reference_set = true;
        }
        else
        {
          add_point(mouse_position(e.button.x, e.button.y));
        }
        break;
      case SDL_BUTTON_RIGHT:
        undo_add_point();
        break;
      case SDL_BUTTON_MIDDLE:
        complex center = mouse_position(e.button.x, e.button.y);
        if (S.s.centering && S.s.inflection > 0)
          center -= S.s.inflections[S.s.inflection - 1];
        S.s.center = center;
        break;
    }
    break;
    }

    case SDL_MOUSEWHEEL:
    {
      if (e.wheel.y > 0)
      {
        zoom(mouse_x, mouse_y, (mods & KMOD_SHIFT) ? 10 : 1);
      }
      if (e.wheel.y < 0)
      {
        zoom(mouse_x, mouse_y, (mods & KMOD_SHIFT) ? -10 : -1);
      }
      break;
    }

    case SDL_MOUSEMOTION:
    {
      mouse_x = e.motion.x;
      mouse_y = e.motion.y;
      break;
    }
  }
}

void draw()
{
  if (S.s.undersample < 1) S.s.undersample = 1;
  int image_width = S.fb_width * S.s.oversample / S.s.undersample;
  int image_height = S.fb_height * S.s.oversample / S.s.undersample;
  if (image_width > 1 << 13) image_width = 1 << 13;
  if (image_height > 1 << 13) image_height = 1 << 13;
  if (image_width < 1 << 4) image_width = 1 << 4;
  if (image_height < 1 << 4) image_height = 1 << 4;
  if (image_width != S.fbo_width || image_height != S.fbo_height)
  {
    // recreate framebuffer
    S.fbo_width = image_width;
    S.fbo_height = image_height;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, S.fbo_width, S.fbo_height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glBindFramebuffer(GL_FRAMEBUFFER, S.fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, S.texture, 0);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, S.fbo);
  glViewport(0, 0, S.fbo_width, S.fbo_height);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glBlitFramebuffer(0, 0, S.fbo_width, S.fbo_height, 0, 0, S.fb_width, S.fb_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void poll_events(SDL_Window *window)
{
  SDL_Event e;
  while (SDL_PollEvent(&e))
  {
    handle_event(window, e);
  }
}

void wait_events(SDL_Window *window)
{
  SDL_Event e;
  if (SDL_WaitEvent(&e))
  {
    handle_event(window, e);
    poll_events(window);
  }
}

extern int main(int argc, char **argv)
{
  fprintf(stderr, "inflector-gadget %s (GPL) 2023 Claude Heiland-Allen\n", inflector_gadget_version_string);

  S.s = default_settings;
  for (int arg = 1; arg < argc; ++arg)
  {
    if (ends_with(argv[arg], ".ig.toml"))
    {
      std::ifstream ifs(argv[arg]);
      ifs >> S.s;
      ifs.close();
    }
    else if (ends_with(argv[arg], ".png"))
    {
      std::string comment = read_png_comment(argv[arg]);
      if (comment != "")
      {
        std::istringstream iss(comment);
        iss >> S.s;
      }
    }
  }

  S.ppm = nullptr;
  S.ppm_width = 0;
  S.ppm_height = 0;
  S.max_count = 0;
  S.time = time(0);
  srand(S.time);
  S.frame = 0;
  S.aindex = 0;
  S.playing = false;
  S.fbo_width = 0;
  S.fbo_height = 0;

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
  {
    const std::string message = "SDL_Init: " + std::string(SDL_GetError());
    if (0 != SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Inflector Gadget", message.c_str(), nullptr))
    {
      std::cerr << argv[0] << ": " << message << std::endl;
    }
    return 1;
  }

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
  SDL_Window *window = SDL_CreateWindow("Inflector Gadget", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, S.s.width, S.s.height, window_flags);
  if (! window)
  {
    const std::string message = "SDL_CreateWindow: " + std::string(SDL_GetError());
    if (0 != SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Inflector Gadget", message.c_str(), nullptr))
    {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s", message.c_str());
    }
    SDL_Quit();
    return 1;
  }

  // decide GL+GLSL versions
  int profile, major, minor;
  // GL 4.3 + GLSL 430
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, profile = SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major = 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor = 3);
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if (! gl_context)
  {
    fprintf(stderr, "could not initialize OpenGL %s %d.%d\n", profile == SDL_GL_CONTEXT_PROFILE_ES ? "ES" : "Core", major, minor);
    const std::string message = "SDL_GL_CreateContext: " + std::string(SDL_GetError());
    if (0 != SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Inflector Gadget", message.c_str(), window))
    {
      SDL_LogCritical(SDL_LOG_CATEGORY_APPLICATION, "%s", message.c_str());
    }
    SDL_Quit();
    return 1;
  }
  SDL_GL_MakeCurrent(window, gl_context);
  fprintf(stderr, "initialized OpenGL %s %d.%d\n", profile == SDL_GL_CONTEXT_PROFILE_ES ? "ES" : "Core", major, minor);

  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew

  SDL_GL_SetSwapInterval(1);
  set_fullscreen(window, S.s.fullscreen);
  S.aspect = dvec2(S.s.width * 1.0 / S.s.height, 1.0);

  S.f = compile_shader("#version 430 core", algorithm_float_plain);
  S.d = compile_shader("#version 430 core\n#define USE_DOUBLE", algorithm_double_plain);
  S.fp = compile_shader("#version 430 core", algorithm_float_perturb);
  S.dp = compile_shader("#version 430 core\n#define USE_DOUBLE", algorithm_double_perturb);

  glGenBuffers(4, &S.ssbo[1]);

  GLuint vao = 0;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  float vdata[8] =
    { -1.0, -1.0
    ,  1.0, -1.0
    , -1.0,  1.0
    ,  1.0,  1.0
    };
  GLuint vbo = 0;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), &vdata[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glGenTextures(1, &S.texture);
  glBindTexture(GL_TEXTURE_2D, S.texture);
  glGenFramebuffers(1, &S.fbo);

  S.running = true;
  while (S.running)
  {
    if (S.playing)
    {
      update_animation();
      if (S.s.inflection > S.max_count) S.max_count = S.s.inflection;
      switch (S.s.algorithm)
      {
        case algorithm_double_perturb: update_shader(S.dp, S.s, S.aspect, S.ssbo); break;
        case algorithm_double_plain:   update_shader(S.d,  S.s, S.aspect, S.ssbo); break;
        case algorithm_float_perturb:  update_shader(S.fp, S.s, S.aspect, S.ssbo); break;
        case algorithm_float_plain:    update_shader(S.f,  S.s, S.aspect, S.ssbo); break;
      }
      draw();
      if (S.recording)
      {
        save_screenshot();
        if (S.frame >= S.aframes)
        {
          if (S.video)
          {
            pclose(S.video);
            S.video = 0;
          }
          S.recording = false;
          S.playing = false;
          S.time = time(0);
          S.frame = 0;
          if (S.is_stop_motion)
          {
            S.s.inflection = S.stop_motion_inflection;
          }
          else
          {
            activate_keyframe();
          }
        }
      }
      SDL_GL_SwapWindow(window);
      poll_events(window);
    }
    else
    {
      if (S.s.inflection > S.max_count) S.max_count = S.s.inflection;
      switch (S.s.algorithm)
      {
        case algorithm_double_perturb: update_shader(S.dp, S.s, S.aspect, S.ssbo); break;
        case algorithm_double_plain:   update_shader(S.d,  S.s, S.aspect, S.ssbo); break;
        case algorithm_float_perturb:  update_shader(S.fp, S.s, S.aspect, S.ssbo); break;
        case algorithm_float_plain:    update_shader(S.f,  S.s, S.aspect, S.ssbo); break;
      }
      draw();
      if (S.screenshot)
      {
        save_screenshot();
        S.screenshot = false;
      }
      SDL_GL_SwapWindow(window);
      wait_events(window);
    }
  }
  free(S.ppm);

  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
