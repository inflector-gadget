const FLOAT mer = 535.491655524765;

uniform bool centering;
uniform bool mandelbrot;
uniform int count;

uniform int iterations;
uniform int samples;

uniform VEC2 center; // offset from reference orbit
uniform VEC2 center0; // offset of reference orbit from image center
uniform VEC2 radius;
uniform VEC2 aspect;

uniform vec4 colours[COLOURS];

layout (std430, binding = 1) buffer er_buffer
{
  // should have count items defined
  FLOAT er[];
};

layout (std430, binding = 2) buffer C_orbit_buffer
{
  // should have count items defined
  VEC4 C_orbit[]; // xy C orbit during inflection; zw C orbit during inflection, minus inflection
};

layout (std430, binding = 3) buffer Z_orbit_buffer
{
  // should have at least iterations items defined
  VEC4 Z_orbit[]; // ZC xy, Z0 zw; orbit starting from C and 0 respectively
};

const uvec2 tile = uvec2(0u, 0u);

in vec2 texcoord;

out vec4 colour;

void main()
{
  uint seed = uint(samples) * interleave(tile.x + uint(gl_FragCoord.x), tile.y + uint(gl_FragCoord.y));
  vec2 jseed = vec2(uhash(2u * seed + 0u), uhash(2u * seed + 1u));
  vec2 dx = dFdx(texcoord);
  vec2 dy = dFdy(texcoord);
  FLOAT px = FLOAT(length(VEC4(radius, radius) * VEC4(dFdx(vec2(aspect) * texcoord), dFdy(vec2(aspect) * texcoord))));
  vec4 colourAccum = vec4(0.0);
  for (int sample_ = 0; sample_ < (samples == 0 ? 1 : samples); ++sample_)
  {
    vec2 jitter = samples == 0 ? vec2(0.0, 0.0) : tent(fract(jseed + vec2(a1, a2) * float(sample_)));
    VEC2 texcoord_ = center + cMul(radius, aspect * VEC2(texcoord + jitter.x * dx + jitter.y * dy));

  vec4 mcolour = vec4(0.0, 0.0, 0.0, 0.0);
  if (mandelbrot)
  {
    VEC4 c = VEC4(center0 + texcoord_, px, 0.0);
    int n = 0;
    VEC4 z = c;
    for (n = 0; n < 1024; ++n)
    {
      if (dot(z.xy, z.xy) >= mer)
        break;
      z = cSqr(z) + c;
    }
    if (dot(z.xy, z.xy) < mer)
    {
      mcolour = colours[COLOUR_MINTERIOR];
    }
    else
    {
      float r = float(length(z.xy));
      float dr = float(length(z.zw));
      float de = 2.0 * r * log(r) / dr;
      float g = tanh(clamp(de, 0.0, 4.0));
      if (isnan(de) || isinf(de) || isnan(dr) || isinf(dr) || isnan(r) || isinf(r) || isnan(g) || isinf(g))
        g = 1.0;
      mcolour = mix(colours[COLOUR_MBOUNDARY], z.y > 0 ? colours[COLOUR_MEXTERIOR1] : colours[COLOUR_MEXTERIOR2], g);
    }
  }
  VEC2 c = texcoord_;

  int i;
  bool escaped = false;
  FLOAT R = 2.0;
  FLOAT R2 = R * R;
  VEC2 Zz = VEC2(0.0, 0.0);
  VEC2 dc = VEC2(px, 0.0);
  if (count > 0)
  {
    for (i = 0; i < count; ++i)
    {
      R2 = er[i];
      VEC2 C_D = C_orbit[i].zw;
      VEC2 C = centering && i == 0 ? C_D : C_orbit[i].xy;
      Zz = C + c;
      if (dot(Zz, Zz) >= R2)
      {
        escaped = true;
        break;
      }
      dc = cMul(2.0 * (C_D + c), dc);
      c = cMul(2.0 * C_D + c, c);
    }
  }
  int n = 0;
  int m = count != 0 ? 0 : 1;
  bool useC = count != 0;
  VEC2 z = c;
  VEC2 dz = dc;
  dc = count != 0 ? VEC2(0.0, 0.0) : dc;
  c = count != 0 ? VEC2(0.0, 0.0) : c;
  if (! escaped)
  {
    for (n = 0; n < iterations && m < Z_orbit.length(); ++n)
    {
      VEC2 Z = useC ? Z_orbit[m].xy : Z_orbit[m].zw;
      Zz = Z + z;
      FLOAT Zz2 = dot(Zz, Zz);
      // check escape
      if (Zz2 >= R2)
      {
        escaped = true;
        break;
      }
      FLOAT z2 = dot(z, z);
      // check rebase to 0 orbit
      if (Zz2 < z2)
      {
        z = Zz;
        m = 0;
        useC = false;
        Z = useC ? Z_orbit[m].xy : Z_orbit[m].zw;
      }
      // iterate
      dz = cMul(2.0 * Zz, dz) + dc;
      z = cMul(2.0 * Z + z, z) + c;
      m = m + 1;
    }
  }
  if (! escaped)
  {
    colourAccum += mix(colours[COLOUR_INTERIOR], vec4(mcolour.rgb, 1.0), mandelbrot ? mcolour.a : 0.0);
    continue;
  }
  float r = float(length(Zz));
  float dr = float(length(dz));
  float de = 2.0 * r * log(r) / dr;
  float g = tanh(clamp(de, 0.0, 4.0));
  if (isnan(de) || isinf(de) || isnan(dr) || isinf(dr) || isnan(r) || isinf(r) || isnan(g) || isinf(g))
    g = 1.0;
  bool decomposition = Zz.y > 0.0;
  vec4 exterior = decomposition ? colours[COLOUR_EXTERIOR1] : colours[COLOUR_EXTERIOR2];
  float rainbow = pow(2.0, clamp(1.0 - log2(log(float(dot(Zz, Zz)))/log(float(R2))), 0.0, 1.0)) - 1.0;
  vec4 erainbow = vec4(mix(mix(vec3(normalize(vec2(Zz)), 0.0), vec3(1.0), 0.5), colours[COLOUR_ACCENT].rgb, colours[COLOUR_ACCENT].a), 1.0);
  exterior = mix(exterior, erainbow, rainbow);
  colourAccum += mix(mix(colours[COLOUR_BOUNDARY], exterior, g), vec4(mcolour.rgb, 1.0), mandelbrot ? mcolour.a : 0.0);

  }
  colour = linear_to_srgb(colourAccum / colourAccum.a);
}
