layout (location = 0) in vec2 point;

out vec2 texcoord;

void main()
{
  texcoord = point;
  gl_Position = vec4(point, 0.0, 1.0);
}
