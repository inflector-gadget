#define COLOUR_INTERIOR 0
#define COLOUR_BOUNDARY 1
#define COLOUR_EXTERIOR1 2
#define COLOUR_EXTERIOR2 3
#define COLOUR_MINTERIOR 4
#define COLOUR_MBOUNDARY 5
#define COLOUR_MEXTERIOR1 6
#define COLOUR_MEXTERIOR2 7
#define COLOUR_ACCENT 8
#define COLOURS 9

vec2 cMul(vec2 a, vec2 b)
{
  return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 cSqr(vec2 a)
{
  return cMul(a, a);
}

vec4 cMul(vec4 a, vec4 b)
{
  return vec4(cMul(a.xy, b.xy), cMul(a.xy, b.zw) + cMul(a.zw, b.xy));
}

vec4 cSqr(vec4 a)
{
  return cMul(a, a);
}

vec2 cSqrt(vec2 z)
{
  float m = length(z);
  float x = sqrt(max(0.0, 0.5 * (m + z.x)));
  float y = sqrt(max(0.0, 0.5 * (m - z.x)));
  return vec2(x, sign(z.y) * y);
}

uint interleave(uint x, uint y)
{
  uint o = 0u;
  for (int bo = 31; bo >= 0; --bo)
  {
    uint bit = 1u << (bo >> 1);
    o <<= 1;
    o |= (((((bo & 1) == 1) ? x : y) & bit) == bit) ? 1u : 0u;
  }
  return o;
}

// http://www.burtleburtle.net/bob/hash/integer.html
uint hash(uint a)
{
  a = (a+0x7ed55d16u) + (a<<12);
  a = (a^0xc761c23cu) ^ (a>>19);
  a = (a+0x165667b1u) + (a<<5);
  a = (a+0xd3a2646cu) ^ (a<<9);
  a = (a+0xfd7046c5u) + (a<<3);
  a = (a^0xb55a4f09u) ^ (a>>16);
  return a;
}

float uhash(uint a)
{
  return float(hash(a)) / pow(2.0, 32.0);
}

// https://github.com/lycium/FractalTracer/blob/da1168615c3a0dfdb3208d0901952d4a2e0df758/src/renderer/Renderer.h#L108
float tent(float v)
{
  float orig = v * 2.0 - 1.0;
  v = orig / sqrt(abs(orig));
  v = v - sign(orig);
  if (isnan(v) || isinf(v))
  {
    v = 0.0;
  }
  return v;
}

vec2 tent(vec2 x)
{
  return vec2(tent(x.x), tent(x.y));
}

const float g2 = 1.32471795724474602596;
const float a1 = 1.0/g2;
const float a2 = 1.0/(g2*g2);

float linear_to_srgb(float c)
{
  c = clamp(c, 0.0, 1.0);
  if (c <= 0.0031308)
  {
    return 12.92 * c;
  }
  else
  {
    return 1.055 * pow(c, 1.0 / 2.4) - 0.055;
  }
}

vec3 linear_to_srgb(vec3 x)
{
  return vec3(linear_to_srgb(x.r), linear_to_srgb(x.g), linear_to_srgb(x.b));
}

vec4 linear_to_srgb(vec4 x)
{
  return vec4(linear_to_srgb(x.rgb), x.a);
}

#if __VERSION__ >= 400

dvec2 cMul(dvec2 a, dvec2 b)
{
  return dvec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

dvec2 cSqr(dvec2 a)
{
  return cMul(a, a);
}

dvec4 cMul(dvec4 a, dvec4 b)
{
  return dvec4(cMul(a.xy, b.xy), cMul(a.xy, b.zw) + cMul(a.zw, b.xy));
}

dvec4 cSqr(dvec4 a)
{
  return cMul(a, a);
}

dvec2 cSqrt(dvec2 z)
{
  double m = length(z);
  double x = sqrt(max(0.0, 0.5 * (m + z.x)));
  double y = sqrt(max(0.0, 0.5 * (m - z.x)));
  return dvec2(x, sign(z.y) * y);
}

#endif

#ifdef USE_DOUBLE

#define FLOAT double
#define VEC2 dvec2
#define VEC4 dvec4

#else

#define FLOAT float
#define VEC2 vec2
#define VEC4 vec4

#endif
