const FLOAT mer = 535.491655524765;

uniform bool centering;
uniform bool mandelbrot;
uniform int count;

uniform int iterations;
uniform int samples;

uniform VEC2 center;
uniform VEC2 radius;
uniform VEC2 aspect;

uniform vec4 colours[COLOURS];

layout (std430, binding = 1) buffer er_buffer
{
  // should have count items defined
  FLOAT er[];
};

layout (std430, binding = 4) buffer inflection_buffer
{
  // should have count items defined
  VEC2 inflection[]; // xy inflection points
};

const uvec2 tile = uvec2(0u, 0u);

in vec2 texcoord;

out vec4 colour;

void main()
{
  uint seed = uint(samples) * interleave(tile.x + uint(gl_FragCoord.x), tile.y + uint(gl_FragCoord.y));
  vec2 jseed = vec2(uhash(2u * seed + 0u), uhash(2u * seed + 1u));
  vec2 dx = dFdx(texcoord);
  vec2 dy = dFdy(texcoord);
  FLOAT px = FLOAT(length(VEC4(radius, radius) * VEC4(dFdx(vec2(aspect) * texcoord), dFdy(vec2(aspect) * texcoord))));
  vec4 colourAccum = vec4(0.0);
  for (int sample_ = 0; sample_ < (samples == 0 ? 1 : samples); ++sample_)
  {
    vec2 jitter = samples == 0 ? vec2(0.0, 0.0) : tent(fract(jseed + vec2(a1, a2) * float(sample_)));
    VEC2 texcoord_ = center + cMul(radius, aspect * VEC2(texcoord + jitter.x * dx + jitter.y * dy));

  VEC4 c = VEC4(texcoord_, px, 0.0);
  vec4 mcolour = vec4(0.0, 0.0, 0.0, 0.0);
  if (mandelbrot)
  {
    int n = 0;
    VEC4 z = c;
    for (n = 0; n < 1024; ++n)
    {
      if (dot(z.xy, z.xy) >= mer)
        break;
      z = cSqr(z) + c;
    }
    if (dot(z.xy, z.xy) < mer)
    {
      mcolour = colours[COLOUR_MINTERIOR];
    }
    else
    {
      float r = float(length(z.xy));
      float dr = float(length(z.zw));
      float de = 2.0 * r * log(r) / dr;
      float g = tanh(clamp(de, 0.0, 4.0));
      if (isnan(de) || isinf(de) || isnan(dr) || isinf(dr) || isnan(r) || isinf(r) || isnan(g) || isinf(g))
        g = 1.0;
      mcolour = mix(colours[COLOUR_MBOUNDARY], z.y > 0 ? colours[COLOUR_MEXTERIOR1] : colours[COLOUR_MEXTERIOR2], g);
    }
  }
  int i;
  bool escaped = false;
  FLOAT R = 2.0;
  FLOAT R2 = R * R;
  if (count > 0)
  {
    for (i = 0; i < count; ++i)
    {
      R2 = er[i];
      if (dot(c.xy, c.xy) >= R2)
      {
        escaped = true;
        break;
      }
      if (centering && i == 0)
      {
        c += VEC4(inflection[0], 0.0, 0.0);
      }
      VEC4 f = VEC4(inflection[i], 0.0, 0.0);
      c = cSqr(c - f) + f;
    }
  }
  int n = 0;
  VEC4 z = count != 0 ? c : VEC4(0.0, 0.0, 0.0, 0.0);
       c = count != 0 ? VEC4(inflection[count - 1], 0.0, 0.0) : c;
  if (! escaped)
  {
    for (n = 0; n < iterations; ++n)
    {
      if (dot(z.xy, z.xy) >= R2)
      {
        escaped = true;
        break;
      }
      z = cSqr(z) + c;
    }
  }
  if (! escaped)
  {
    colourAccum += mix(colours[COLOUR_INTERIOR], vec4(mcolour.rgb, 1.0), mandelbrot ? mcolour.a : 0.0);
    continue;
  }
  float r = float(length(z.xy));
  float dr = float(length(z.zw));
  float de = 2.0 * r * log(r) / dr;
  float g = tanh(clamp(de, 0.0, 4.0));
  if (isnan(de) || isinf(de) || isnan(dr) || isinf(dr) || isnan(r) || isinf(r) || isnan(g) || isinf(g))
    g = 1.0;
  bool decomposition = z.y > 0.0;
  vec4 exterior = decomposition ? colours[COLOUR_EXTERIOR1] : colours[COLOUR_EXTERIOR2];
  float rainbow = pow(2.0, clamp(1.0 - log2(log(float(dot(z.xy, z.xy)))/log(float(R2))), 0.0, 1.0)) - 1.0;
  vec4 erainbow = vec4(mix(mix(vec3(normalize(z.xy), 0.0), vec3(1.0), 0.5), colours[COLOUR_ACCENT].rgb, colours[COLOUR_ACCENT].a), 1.0);
  exterior = mix(exterior, erainbow, rainbow);
  colourAccum += mix(mix(colours[COLOUR_BOUNDARY], exterior, g), vec4(mcolour.rgb, 1.0), mandelbrot ? mcolour.a : 0.0);

  }
  colour = linear_to_srgb(colourAccum / colourAccum.a);
}
