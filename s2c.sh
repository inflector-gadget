#!/bin/bash
echo "const char *$1 ="
# strip multiline /* .. */ comments, followed by // .. EOL comments, very hacky
tr '\n' '@' |
sed 's|/\*[^*]*\*/|\n|g' |
sed 's|//[^@]*|\n|g' |
tr '@' '\n' |
sed 's/^ *//g' |
tr -s '\n ' |
sed 's/ = /=/g' |
sed 's|\\|\\\\|g' |
sed 's|"|\\"|g' |
sed 's|^\(\#.*\)$|\\n\1\\n|' |
sed 's|^|"|' |
sed 's|$|"|'
echo '"\n"'
echo ";"
