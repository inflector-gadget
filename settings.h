#pragma once

#include <iostream>
#include <map>
#include <vector>
#include <glm/glm.hpp>
using namespace glm;

#include "complex.h"

#define SSBO_ER 1
#define SSBO_C_ORBIT 2
#define SSBO_Z_ORBIT 3
#define SSBO_INFLECTION 4

#define COLOUR_INTERIOR 0
#define COLOUR_BOUNDARY 1
#define COLOUR_EXTERIOR1 2
#define COLOUR_EXTERIOR2 3
#define COLOUR_MINTERIOR 4
#define COLOUR_MBOUNDARY 5
#define COLOUR_MEXTERIOR1 6
#define COLOUR_MEXTERIOR2 7
#define COLOUR_ACCENT 8
#define COLOURS 9

struct theme
{
  vec4 colours[COLOURS];
};

bool operator == (const struct theme &a, const struct theme &b);

extern const struct theme lighttheme;
extern const struct theme darktheme;

enum shader_algorithm
{ algorithm_float_plain = 0
, algorithm_double_plain = 1
, algorithm_float_perturb = 2
, algorithm_double_perturb = 3
};

struct settings
{
  complex center;
  double radius;
  int64_t iterations;
  int64_t samples;
  bool fullscreen;
  int64_t width; // in screen coordinates (window size)
  int64_t height;
  int64_t oversample; // resampling ratio (decouple image size from window size)
  int64_t undersample;
  bool centering;
  bool mandelbrot;
  enum shader_algorithm algorithm;
  bool reference_set;
  complex reference;
  int64_t theme;
  std::vector<struct theme> themes;
  int64_t inflection; // must be less or equal to inflections.size()
  std::vector<complex> inflections; // must not grow beyond MAXCOUNT
  // key frames
  int64_t keyframe;
  std::vector<std::vector<complex>> keyframes;
  double speed;
};

extern const struct settings default_settings;

std::istream & operator >> (std::istream &ifs, struct settings &s);
std::ostream & operator << (std::ostream &ofs, const struct settings &p);
